const input = document.querySelector('input');
const price= document.querySelector('.show_input');
const price_value = document.querySelector('.value');
const close = document.querySelector('.close');



input.addEventListener( 'focus', () => {
    input.className = 'focused';
});

input.addEventListener( 'blur', () => {
    if (input.value < 0 || input.value === '') {
        input.value = '';
        input.className = 'error';
        let extraMessage = document.createElement('p');
        extraMessage.innerText = 'Please enter correct price';
        input.after(extraMessage);

    }
    else {
    input.className = 'unfocused';
    price.style.display = 'block';
    price_value.innerHTML = 'Current price is: ' + `${input.value}`;
}
});

close.onclick = ()=> {
    price.style.display = 'none';
    input.value = '';
   };

